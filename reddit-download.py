#!/usr/bin/env python3
import praw
import sys
import re
import requests
import tempfile
import subprocess
import os
from ffmpy import FFmpeg


def download(url, filename):
    r = requests.get(url, allow_redirects=True)
    open(filename, 'wb').write(r.content)
    return filename


def main():
    if len(sys.argv) < 2:
        print("require url")
        return


    reddit = praw.Reddit(
            client_id="UaigsVIIgPxVTTYTag9DyA",
            client_secret=None,
            user_agent="vv/0.0.1"
    )
    
    submission = reddit.submission(url=sys.argv[1])
    
    video_url = submission.media['reddit_video']['fallback_url']
    audio_url = re.sub(r"_(\d+)", "_audio", video_url)

    _, video_path = tempfile.mkstemp()
    _, audio_path = tempfile.mkstemp()
    combined_path = os.path.join('~/storage/downloads', 'reddit-video-downloads', f"{submission.id}.mp4")
    combined_path = os.path.expanduser(combined_path)

    os.makedirs(os.path.dirname(combined_path), exist_ok=True)

    download(video_url, video_path)
    download(audio_url, audio_path)

    ff = FFmpeg(
            inputs={video_path: None, audio_path: None},
            outputs={combined_path: '-c copy'},
            global_options=['-y']
    )

    ff.run()
    subprocess.run(['termux-open', combined_path])




if __name__ == '__main__':
    main()
